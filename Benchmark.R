library(tidyverse)

load(file ="./data/clean_train_test.Rda")

# GLM ---------------------------------------------------------------------

## Fit a GLMMET
library(glmnet)

train2 <- train_set %>% 
  mutate_if(is.character, factor) %>% 
  select(-log_sp, -Id)

x <- model.matrix(SalePrice ~ ., train2)[, -1]
y <- train2$SalePrice
cv_out <- cv.glmnet(x = x,
                     y = y,
                     family = "poisson",
                     alpha = 1)
plot(cv_out)

# Min value of lambda
lambda_min <- cv_out$lambda.min

# Best value of lambda
lambda_1se <- cv_out$lambda.1se

# Regression coefficients
coef(cv_out, s = lambda_1se)

pred <- exp(predict(cv_out, newx = x, s = lambda_1se))

Metrics::rmse(train2$SalePrice, pred)
Metrics::mae(train2$SalePrice, pred)
Metrics::mape(train2$SalePrice, pred)
plot(train2$SalePrice, pred)
abline(a=0,b=1)

# Caret

tr_ctrl <- trainControl(method = "cv") # 10 kFold

cv_glmnet_res <- train(SalePrice ~ ., 
                    data = train2,
                    method = "glmnet",
                    preProcess = c("scale", "center", "spatialSign"),
                    family = "poisson",
                    tuneLength = 5)

pred <- exp(predict(cv_glmnet_res, newdata = train2, type = "raw"))
Metrics::rmse(train2$SalePrice, pred)
Metrics::mae(train2$SalePrice, pred)
Metrics::mape(train2$SalePrice, pred)
plot(train2$SalePrice, pred)
abline(a=0,b=1)

## FIT A GLM
glm_fit <- glm(SalePrice ~ ., data = train2, 
               family = Gamma(link = "log"))

pred <- predict(glm_fit, newdata = train2, type = "response")
Metrics::rmse(train2$SalePrice, pred)
Metrics::mae(train2$SalePrice, pred)
Metrics::mape(train2$SalePrice, pred)
mi_R2(pred, sp)
plot(train2$SalePrice, pred)
abline(a=0,b=1)

# CV
train3 <- train2 %>% mutate_at(n_numeric[-29], scale)
cv_glm_res <- cv_glm(SalePrice ~ ., data_tr = train3, family = Gamma(link = "log"))

require(gamlss)
cv_gamlss_res <- cv_gamlss(SalePrice ~ ., data_tr = train3)
detach("package:gamlss", unload=TRUE)
detach("package:gamlss.dist", unload=TRUE)
detach("package:gamlss.data", unload=TRUE)

# Caret

tr_ctrl <- trainControl(method = "cv") # 10 kFold

cv_glm_res <- train(SalePrice ~ ., 
                    data = train2,
                    method = "glm",
                    preProcess = c("scale", "center", "spatialSign"),
                    family = Gamma(link = "log"))
pred <- predict(cv_glm_res, newdata = train2, type = "raw")
Metrics::rmse(train2$SalePrice, pred)
Metrics::mae(train2$SalePrice, pred)
Metrics::mape(train2$SalePrice, pred)
plot(train2$SalePrice, pred)
abline(a=0,b=1)

## RF
train2 <- train_set %>% 
  mutate_if(is.character, factor) %>% 
  select(-Id)
test2 <- test_set %>% 
  mutate_if(is.character, factor) %>% 
  select(-Id)

cv_rf_result <- cv_rf(log_sp ~ ., data = train2)

rf_fit <- randomForest(log_sp ~ ., data = train2)
rf_fit
t.test(unlist(cv_rf_result$R2))
t.test(unlist(cv_rf_result$RMSE))
