library(tidyverse)
library(dplyr)

load(file ="./data/clean_train_test.Rda")

# GLM ---------------------------------------------------------------------

### GLMNET
library(glmnet)

train2 <- train_set %>% 
  select(-log_sp, -Id)

x <- model.matrix(SalePrice ~ ., train2)[, -1]
y <- train2$SalePrice

## Poisson, lasso
cv_poiss_lasso <- cv.glmnet(x = x,
                    y = y,
                    family = "poisson",
                    alpha = 1)
plot(cv_poiss_lasso)

# Min value of lambda
lambda_min <- cv_poiss_lasso$lambda.min

# Best value of lambda
lambda_1se <- cv_poiss_lasso$lambda.1se

# Regression coefficients
coef(cv_poiss_lasso, s = lambda_1se)

pred <- predict(cv_poiss_lasso, newx = x, s = lambda_1se)

mi_R2(log(train2$SalePrice), pred)
Metrics::rmse(log(train2$SalePrice), pred)
Metrics::mae(log(train2$SalePrice), pred)
Metrics::mape(log(train2$SalePrice), pred)

plot(log(train2$SalePrice), pred)
abline(a=0,b=1)

## GAMLSS LOGNORMAL
require(gamlss)
cv_logno <- gamlssCV(SalePrice ~ ., 
                             data = train2,
                            family = LOGNO(),
                            alpha = 1)
